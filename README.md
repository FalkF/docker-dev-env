# Docker Dev Env - Ember

A dockerized development environment for Ember.js

This repository can be used to gradually learn docker.
The complexity should increase with each commit.
At first only basic docker commands are used, later tools like `docker-compose` are introduced.


## Setup

### Install Docker
- just do it
### Build Image
- ```docker build -t ember-docker:0.1.0 .```
- ```docker images ember-docker```
### ~~Create Container~~
Not needed since docker run always creates a new container.
Feel free to do it anyway.
- ```docker create ember-docker:0.1.0```
- ```docker container ls -a``` … `-a` is need to see stopped containers
### Initialize Ember
Follow the steps or pull your repo.

`docker run` is used to create a container and attach to it.
`-v $(pwd):/workspace ` mounts the current folder to the workspace.
`-u` dont save files as root
- ```docker run -u $(id -u) -it -v $(pwd):/workspace ember-docker:0.1.0 bash```
- ```ember new awesome-app```

exit the container with with `ctrl + d` or `exit`
- ```chown $(id -u) awesome-app```


## Work

### Ember Server
- ```docker run -v $(pwd)/awesome-app:/workspace -p 4200:4200 ember-docker:0.1.0```
### Ember and node related commands
- ```docker run -v $(pwd)/awesome-app:/workspace ember-docker:0.1.0 bash```

