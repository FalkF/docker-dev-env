# heavily inspired by https://hub.docker.com/r/danlynn/ember-cli/dockerfile

FROM node:latest

WORKDIR /workspace

# ember server on port 4200
# test server on port 7357
EXPOSE 4200 7357

# uncomment when the ember project is intialized
# allows to leave the -v $(pwd)/awesome-app:/workspace out of docker run
# allows to use create and start
#VOLUME /awesome-app /workspace

# run ember server on container start
CMD ["ember", "server"]

# Install watchman build dependencies
RUN \
	apt-get update -y
#	apt-get install -y python-dev

# install watchman
# Note: See the README.md to find out how to increase the
# fs.inotify.max_user_watches value so that watchman will
# work better with ember projects.
#RUN \
#	git clone https://github.com/facebook/watchman.git &&\
#	cd watchman &&\
#	git checkout v4.9.0 &&\
#	./autogen.sh &&\
#	./configure &&\
#	make &&\
#	make install

# install bower
#RUN \
#	yarn global add bower

# install chrome for default testem config (as of ember-cli 2.15.0)
#RUN \
#    apt-get update &&\
#    apt-get install -y \
#        apt-transport-https \
#        gnupg \
#        --no-install-recommends &&\
#	curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - &&\
#	echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list &&\
#	apt-get update &&\
#	apt-get install -y \
#	    google-chrome-stable \
#	    --no-install-recommends

# tweak chrome to run with --no-sandbox option
#RUN \
#	sed -i 's/"$@"/--no-sandbox "$@"/g' /opt/google/chrome/google-chrome

# set container bash prompt color to blue in order to
# differentiate container terminal sessions from host
# terminal sessions
#RUN \
#	echo 'PS1="\[\\e[0;94m\]${debian_chroot:+($debian_chroot)}\\u@\\h:\\w\\\\$\[\\e[m\] "' >> ~/.bashrc

# install ember-cli
RUN \
	yarn global add ember-cli
